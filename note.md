[
  '{{repeat(100, 100)}}',
  {
    title: '{{lorem(integer(1, 10), "words")}}',
    author: '{{firstName()}} {{surname()}}',
    content: '{{lorem(integer(1, 10), "paragraph")}}',
    tags: function(tags) {

		var temp = new Array();

		var iContent = this.content;

        temp = iContent.split(" ");

      	var collection = new Array();

      	var length = temp.length;


      	var x = Math.floor((Math.random() * 10) + 1);

      	for (i=0;i<x;i++) {

          	var j = Math.floor((Math.random() * 100) + 1);

        	collection[i] = temp[j];

        }

		return collection;
    },
    url: function(url) {

      var title = this.title;

      var title_url = title.split(" ").join("-");

      return 'http://domain.tld/book/' + title_url;
    }
  }
]