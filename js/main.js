(function($) {

    var collection = [];

    var index = lunr(function () {
        this.field('title', {boost: 5})
        this.field('author', {boots: 7})
        this.field('content')
        this.field('tags', {boost: 10})
        this.ref('id')
    });

    $.get('/data.json', function(response) {

        appendData(response, index);

    });

    function appendData(response, index) {

        $.each(response, function(k, v) {

            index.add({

                id: k,
                title: v.title,
                author: v.author,
                content: v.content,
                tags: v.tags.toString()

            });

            collection[k] = {

                id: k,
                title: v.title,
                author: v.author,
                content: v.content,
                tags: v.tags

            }



        });

    }

    $('#search-me').on('keyup', function(e) {

        $('.search-wrapper').animate({

            top: "10%"

        }, 300);

        $('#results').html('');

        var search = index.search(this.value);

        $.each(search, function(k, v) {

            var tpl = "<h2>{{ title }}</h2>" +
                        "<p class='tags'><strong>Tags :</strong> {{#tags}}<span>{{.}}</span>{{/tags}}</p>" +
                        "<i><strong>Author :</strong> {{ author }}</i>" +
                        "<hr>" +
                        "<p>{{ content }}</p>"


            var html = Mustache.to_html(tpl, collection[v.ref]);

            $('#results').append(html);

        });

    });

})(jQuery);
